import { getHotnews, getHotnewsCategory, insert_update_hotnews, insert_update_hotnews_category } from '@/api/hotnews_manager'
import { Message } from 'element-ui'

const state = {
   hotnewslist:[],
   hotnewscategorylist: []
}

const mutations = {
    SET_HOTNEWS: (state, hotnewslist) => {
        state.hotnewslist = hotnewslist
    },
    SET_HOTNEWS_CATEGORY: (state, hotnewscategorylist) => {
        state.hotnewscategorylist = hotnewscategorylist
    }
}

const actions = {
    // 得知所有最新消息
    getHotnews({ commit }, query) {
        return new Promise((resolve, reject) => {
            console.log(query)
            getHotnews(query).then(response => {
                commit('SET_HOTNEWS', response.data.list)
                resolve(response.data.list)
            }).catch(error => {
                console.log("error", error)
                reject(error)
            })
        })
    },
    // 得知所有最新消息分類
    getHotnewsCategory({ commit }, page) {
        return new Promise((resolve, reject) => {
            getHotnewsCategory(page).then(response => {
                // console.log("test", response)
                commit('SET_HOTNEWS_CATEGORY', response.data.list)
                resolve(response.data.list)
            }).catch(error => {
                console.log("error", error)
                reject(error)
            })
        })
    },
    // 新增及修改所有最新消息
    insert_update_hotnews({ commit }, data){
        return new Promise((resolve, reject) => {
            insert_update_hotnews(data).then(response => {
                Message({
                    message: response.message,
                    type: response.success == false ? 'error' : 'success',
                    duration: 1500
                })
                resolve(response)
            }).catch(error => {
                console.log("error", error)
                reject(error)
            })
        })
    },

    insert_update_hotnews_category({ commit }, data) {
        return new Promise((resolve, reject) => {
            insert_update_hotnews_category(data).then(response => {
                Message({
                    message: response.message,
                    type: 'success',
                    duration: 1500
                })
                resolve(response)
            }).catch(error => {
                console.log("error", error)
                reject(error)
            })
        })
    }

}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
