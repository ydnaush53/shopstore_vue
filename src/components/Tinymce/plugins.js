// Any plugins you want to use has to be imported
// Detail plugins list see https://www.tinymce.com/docs/plugins/
// Custom builds see https://www.tinymce.com/download/custom-builds/

const plugins = ['autolink lists textcolor link image charmap anchor searchreplace visualblocks code fullscreen insertdatetime media table contextmenu paste wordcount']

export default plugins
