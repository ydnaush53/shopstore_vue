import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken,removeToken } from '@/utils/auth'
import router from '../router/index'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API_TRANSFER, // url = base url + request url
  withCredentials: false, // send cookies when cross-domain requests
  timeout: 100000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent

    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      // config.headers['X-Token'] = getToken()
      config.headers['Authorization'] = 'Bearer ' + getToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log("errorsw", error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data
    // if the custom code is not 20000, it is judged as an error.
    if (res.code == 20000 && res.success == false) {
      // Message({
      //   message: res.message || 'Error',
      //   type: res.success == false ? 'error' : 'success',
      //   duration: 3500
      // })
      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.code === 50008 || res.code === 50012) {
        // to re-login
        MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
          confirmButtonText: 'Re-Login',
          cancelButtonText: 'Cancel',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return res
      // return Promise.reject(new Error(res.message || 'Error'))
    }
    else if ((res.code == 429 && res.success == false) || (res.code === 50014 && res.success == false)){
      //請求過於頻繁跳轉或是驗證碼過期
      if (res.message !='沒有驗證碼，無法進行查看'){
        Message({
          message: res.message || 'Error',
          type: 'error',
          duration: 2000
        })
        store.dispatch('user/resetToken').then(() => {
          location.reload()
        })
      }
      else{
        store.dispatch('user/resetToken')
      }
      // if (to.path =='d/404'){
      //   router.push(`/login?redirect=dashboard`)
      // }
      // console.log(to.path)
      router.push(`/login?redirect=${to.path}`)
      // router.push(`/login?redirect=${router.fullPath}`)
    }
    else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 2 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
