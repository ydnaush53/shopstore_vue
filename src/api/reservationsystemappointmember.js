import request from '@/utils/request'


//得知預約會員資料
export function getAppointMemberList(query) {
  console.log('get_appoint_member_list', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/appoint_member_record/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/appoint_member_record/' + query,
      method: 'get'
    })
  }
}

//新增、修改預約會員資料
export function postAppointMemberList(data) {
  console.log('post_appoint_member_list', typeof (data))
    return request({
      url: '/api/appoint_member_record',
      method: 'post',
      data
    })
}
