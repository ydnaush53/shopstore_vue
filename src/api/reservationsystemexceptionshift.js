import request from '@/utils/request'


//得知例外排班基本資料
export function getExceptionShiftList(query) {
  console.log('get_exception_shift_list', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/exception_schedule_shift_record/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/exception_schedule_shift_record/' + query,
      method: 'get'
    })
  }
}

//新增、修改例外排班基本資料
export function postExceptionShiftList(data) {
  console.log('post_exception_shift_list', typeof (data))
    return request({
      url: '/api/exception_schedule_shift_record',
      method: 'post',
      data
    })
}

//多筆修改排休狀態
export function BatchExceptionScheduleShiftStatus(data) {
  console.log('batch_exception_schedule_shift_record', typeof (data))
    return request({
      url: '/api/batch_exception_schedule_shift_record',
      method: 'post',
      data
    })
}
