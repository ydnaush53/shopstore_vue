import request from '@/utils/request'


//得知單位管理(banner、網頁資訊)
export function getUnitBannerMetaList(query) {
  console.log('get_unit_banner_meta_list', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/index_unit_banner_meta_record/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/index_unit_banner_meta_record/' + query,
      method: 'get'
    })
  }
}

//新增、修改單位管理(banner、網頁資訊)
export function postUnitBannerMetaList(data) {
  console.log('post_unit_banner_meta_list', typeof (data))
    return request({
      url: '/api/index_unit_banner_meta_record',
      method: 'post',
      data
    })
}
