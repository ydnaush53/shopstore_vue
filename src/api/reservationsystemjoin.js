import request from '@/utils/request'


//得知預約報到資料
export function getAppointJoinList(query) {
  console.log('get_appoint_join_record', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/appoint_join_record/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/appoint_join_record/' + query,
      method: 'get'
    })
  }
}

//修改預約報到資料
export function postAppointJoinList(data) {
  console.log('appoint_join_record_list', typeof (data))
    return request({
      url: '/api/appoint_join_record',
      method: 'post',
      data
    })
}
//多筆修改預約報到狀態
export function BatchAppointJoinStatus(data) {
  console.log('batch_join_status', typeof (data))
    return request({
      url: '/api/batch_join_status_record',
      method: 'post',
      data
    })
}
