import request from '@/utils/request'

export function getMemberGroup(page,query) {
    console.log("getMemberGroup",typeof(query))
    return request({
        url: '/api/linemessage/' + query + '/' + page,
        method: 'get',
    })
}

//得知合作夥伴下群組
export function getMemberGroupName(query) {
    // console.log(data);
    return request({
        url: '/api/linemessagebypid/'+query,
        method: 'get',
    })
}

//新增MemberGroup
export function insertMemberGroup(data){
    return request({
        url: '/api/linemessage',
        method: 'post',
        data
    })
}

//更新MemberGroup
export function updateMemberGroup(data,id) {
    return request({
        url: '/api/linemessage/'+id,
        method: 'post',
        data
    })
}
