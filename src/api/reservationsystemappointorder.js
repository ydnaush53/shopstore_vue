import request from '@/utils/request'


//得知預約訂單資料
export function getAppointOrderList(query) {
  console.log('get_appoint_order_list', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/appoint_order_record/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/appoint_order_record/' + query,
      method: 'get'
    })
  }
}

//新增、修改預約訂單資料
export function postAppointOrderList(data) {
  console.log('post_appoint_order_list', typeof (data))
    return request({
      url: '/api/appoint_order_record',
      method: 'post',
      data
    })
}
//多筆修改預約訂單狀態
export function BatchAppointOrderStatus(data) {
  console.log('batch_appoint_order_status', typeof (data))
    return request({
      url: '/api/batch_order_status_record',
      method: 'post',
      data
    })
}
