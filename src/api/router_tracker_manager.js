import request from '@/utils/request'

export function getRealtimeRouterTracker(query) {
    console.log('get_realtime_router_tracker_manager', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
        url: '/api/realtime_router_tracker_manager/' + query,
      method: 'get'
    })
  } else {
    return request({
        url: '/api/realtime_router_tracker_manager/' + query,
      method: 'get'
    })
  }
}

export function getHistoryRouterTracker(query) {
    console.log('get_history_router_tracker_manager', typeof (query))
    // console.log("get_admin", query)
    if (typeof (query) === 'number') {
        return request({
            url: '/api/history_router_tracker_list_manager/' + query,
            method: 'get'
        })
    } else {
        return request({
            url: '/api/history_router_tracker_list_manager/' + query,
            method: 'get'
        })
    }
}


