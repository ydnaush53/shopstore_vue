import request from '@/utils/request'


//得知預約會員標籤資料
export function getAppointMemberTagList(query) {
  console.log('get_appoint_membertag_list', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/appoint_member_tag_record/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/appoint_member_tag_record/' + query,
      method: 'get'
    })
  }
}

//新增、修改預約會員標籤資料
export function postAppointMemberTagList(data) {
  console.log('post_appoint_member_tag_list', typeof (data))
    return request({
      url: '/api/appoint_member_tag_record',
      method: 'post',
      data
    })
}
