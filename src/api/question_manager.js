import request from '@/utils/request'

export function getQuestion(query) {
    console.log("get_question",typeof(query))
    // console.log("get_question", query)
    if(typeof(query)=="number"){
        return request({
            url: '/api/question/' + query,
            method: 'get',
        })
    }
    else{
        return request({
            url: '/api/question/' + query,
            method: 'get',
        })
    }
}
export function getQuestionCategory(page) {
    return request({
        url: '/api/question_category/' + page,
        method: 'get',
    })
}
export function insert_update_question(data) {
  // console.log(data);
    return request({
        url: '/api/question',
        method: 'post',
        data
    })
}
export function insert_update_question_category(data) {
    // console.log(data);
    return request({
        url: '/api/question_category',
        method: 'post',
        data
    })
}