import request from '@/utils/request'

//取得所有聊天室列表
export function getChatList(query) {
    console.log('get_chat_list', typeof (query))
  // console.log("get_admin_roles", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/chat_list/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/chat_list/' + query,
      method: 'get'
    })
  }
}

//取得所有聊天室要聊天對象的ID列表
export function getChatMemberIDList(query) {
    console.log('get_chat_member_id_list', typeof (query))
    // console.log("get_admin_roles", query)
    if (typeof (query) === 'number') {
        return request({
            url: '/api/chat/member/' + query,
            method: 'get'
        })
    } else {
        return request({
            url: '/api/chat/member/' + query,
            method: 'get'
        })
    }
}

export function insert_update_chat(data) {
    // console.log(data);
    return request({
        url: '/api/chat/member',
        method: 'post',
        data
    })
}