import request from '@/utils/request'


//得知預約排程資料
export function getAppointScheduleList(query) {
  console.log('get_appoint_schedule_list', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/appoint_schedule_record/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/appoint_schedule_record/' + query,
      method: 'get'
    })
  }
}

//新增、修改預約排程資料
export function postAppointScheduleList(data) {
  console.log('post_appoint_schedule_list', typeof (data))
    return request({
      url: '/api/appoint_schedule_record',
      method: 'post',
      data
    })
}

//檢查是否有排班紀錄（帶入日期區間、合作夥伴代號、帶入類型、課程治療師名稱）
export function checkAppointScheduleList(data) {
  console.log('check_appoint_schedule_list', typeof (data))
    return request({
      url: '/api/check_appoint_schedule_record',
      method: 'post',
      data
    })
}
//刪除預約排程資料(該筆資料、該筆以後資料、該筆以前資料、該筆全部資料)
export function deleteAppointSchedule(data) {
  console.log('delete_appoint_schedule_record', typeof (data))
    return request({
      url: '/api/delete_appoint_schedule_record',
      method: 'post',
      data
    })
}
