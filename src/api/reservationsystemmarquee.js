import request from '@/utils/request'


//得知首頁跑馬燈
export function getMarqueeList(query) {
  console.log('get_marquee_list', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/index_marquee_record/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/index_marquee_record/' + query,
      method: 'get'
    })
  }
}

//新增、修改首頁跑馬燈
export function postMarqueeList(data) {
  console.log('post_marquee_list', typeof (data))
    return request({
      url: '/api/index_marquee_record',
      method: 'post',
      data
    })
}
