import request from '@/utils/request'

// 得知聊天機器人基本資料
export function getLineBotInfo(data) {
    return request({
        url: '/api/linebot_get',
        method: 'post',
        data
    })
}
//新增或更新聊天機器人基本資料
export function updateLineBotInfo(data){
    console.log(data)
    return request({
        url: '/api/linebot_update',
        method: 'post',
        data
    })
}

// 得知聊天機器人選單
export function getLineBotMenu(data) {
    return request({
        url: '/api/get_rich_menu',
        method: 'post',
        data
    })
}

// 得知會員標籤下
export function getLineBotMemberTag(query) {
    console.log("get_linebot_member_tag",typeof(query))
    // console.log("get_admin", query)
    if(typeof(query)=="number") {
        return request({
            url: '/api/get_member_tag_2_all_list/' + query,
            method: 'get',
        })
    }
    else {
        return request({
            // url: '/api/get_member_tag_2_all_list/' + '1' + '?' + query,
            url: '/api/get_member_tag_2_all_list/' + query,
            method: 'get',
        })
    }
}

// 得知聊天機器人推送紀錄
export function getLineBotPushMessageList(query) {
    console.log("get_check_linebot_push_message",typeof(query))
    // console.log("get_admin", query)
    if(typeof(query)=="number") {
        return request({
            url: '/api/check_linebot_push_message/' + query,
            method: 'get',
        })
    }
    else {
        return request({
            // url: '/api/check_linebot_push_message/' + '1' + '?' + query,
            url: '/api/check_linebot_push_message/' + query,
            method: 'get',
        })
    }
}

// 更新聊天機器人選單
export function uploadMenu(data) {
    return request({
        url: '/api/build_rich_menu',
        method: 'post',
        data
    })
}

//得知聊天機器人分析數據
export function getLineBotStatic(data){
    return request({
        url: '/api/linebot_ai_data',
        method: 'post',
        data
    })
}

// 查詢linebot下的userid（簡易模式）
export function getLineBotUserIdGet(data) {
    return request({
        url: '/api/linebot_userid_get',
        method: 'post',
        data
    })
}

// 查詢linebot下的userid(進階模式)
export function getLineBotUserIdGet2(query) {
    console.log("get_line_bot_user_id_get_2",typeof(query))
    if(typeof(query)=="number") {
        return request({
            url: '/api/get_member_tag_2/' + query,
            method: 'get',
        })
    }
    else {
        return request({
            // url: '/api/get_member_tag_2/' + '1' + '?' + query,
            url: '/api/get_member_tag_2/' + query,
            method: 'get',
        })
    }
}

// 發送Linebot推送訊息
export function getLineBotPushMessage(data) {
    return request({
        url: '/api/linebot_push_message',
        method: 'post',
        data
    })
}

//搜尋與查看電子發票字軌配號
export function getReceptionSetting(query) {
    console.log("get_reception_setting", typeof (query))
    // console.log("get_question", query)
    return request({
        url: '/api/check_reception_setting/' + query,
        method: 'get',
    })
}

//查詢電子發票配號結果
export function checkReceptionResult(data){
    return request({
        url:'/api/check_reception_setting_result',
        method: 'post',
        data
    })
}

//設定電子發票字軌配號
export function setReceptionResult(data) {
    return request({
        url: '/api/set_reception_setting_result',
        method: 'post',
        data
    })
}

//設定電子發票字軌配號
export function statusReceptionResult(data) {
    return request({
        url: '/api/status_reception_setting_result',
        method: 'post',
        data
    })
}


