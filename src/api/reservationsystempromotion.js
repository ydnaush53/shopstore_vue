import request from '@/utils/request'

export function getPromotionTemplate(query) {
    console.log("get_promotion_template",typeof(query))
    // console.log("get_question", query)
    if(typeof(query)=="number"){
        return request({
            url: '/api/appoint_promotion_ads_template_record/' + query,
            method: 'get',
        })
    }
    else{
        return request({
            url: '/api/appoint_promotion_ads_template_record/' + query,
            method: 'get',
        })
    }
}

export function postPromotionTemplate(data) {
    console.log('post_promotion_template', typeof (data))
    return request({
        url: '/api/appoint_promotion_ads_template_record',
        method: 'post',
        data
    })
}

export function getPromotionReserve(query) {
    console.log("get_promotion_reserve",typeof(query))
    // console.log("get_question", query)
    if(typeof(query)=="number"){
        return request({
            url: '/api/appoint_promotion_ads_reserve_record/' + query,
            method: 'get',
        })
    }
    else{
        return request({
            url: '/api/appoint_promotion_ads_reserve_record/' + query,
            method: 'get',
        })
    }
}

export function postPromotionReserve(data) {
    console.log('post_promotion_template', typeof (data))
    return request({
        url: '/api/appoint_promotion_ads_reserve_record',
        method: 'post',
        data
    })
}