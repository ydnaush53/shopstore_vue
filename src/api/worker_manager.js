import request from '@/utils/request'

export function getWorker(query) {
  console.log('get_worker', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
        url: '/api/worker_manager/' + query,
      method: 'get'
    })
  } else {
    return request({
        url: '/api/worker_manager/' + query,
      method: 'get'
    })
  }
}
export function getSingleWorker(query) {
    console.log('get_worker_single_manager', typeof (query))
    // console.log("get_admin", query)
    if (typeof (query) === 'number') {
        return request({
            url: '/api/worker_single_manager/' + query,
            method: 'get'
        })
    } else {
        return request({
            url: '/api/worker_single_manager/' + query,
            method: 'get'
        })
    }
}
export function getSingleWorkerStatic(query) {
  console.log('get_worker_single_manager_static', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/worker_single_manager_static_data/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/worker_single_manager_static_data/' + query,
      method: 'get'
    })
  }
}


