import request from '@/utils/request'


//得知首頁大圖輪播
export function getBannerList(query) {
  console.log('get_banner_list', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/index_banner_record/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/index_banner_record/' + query,
      method: 'get'
    })
  }
}

//新增、修改首頁大圖輪播
export function postBannerList(data) {
  console.log('post_banner_list', typeof (data))
    return request({
      url: '/api/index_banner_record',
      method: 'post',
      data
    })
}
