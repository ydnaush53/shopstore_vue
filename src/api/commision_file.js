import request from '@/utils/request'

export function getAllCommisionFile(data) {
    console.log("get_commision_file",typeof(data))
    return request({
        url: '/api/commision/get_file',
        method: 'post',
        data
    })
}