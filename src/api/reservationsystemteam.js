import request from '@/utils/request'


//得知團隊成員
export function getTeamList(query) {
  console.log('get_team_member_list', typeof (query))
  // console.log("get_admin", query)
  if (typeof (query) === 'number') {
    return request({
      url: '/api/appoint_team_member_record/' + query,
      method: 'get'
    })
  } else {
    return request({
      url: '/api/appoint_team_member_record/' + query,
      method: 'get'
    })
  }
}

//新增、修改團隊成員
export function postTeamList(data) {
  console.log('post_team_member_list', typeof (data))
    return request({
      url: '/api/appoint_team_member_record',
      method: 'post',
      data
    })
}
